**WHY WEATHER APP?**

As a part of React-native learnings I developed this app. Concepts of Fetch, Modals, ActivitIndicator, SafeAreaView from React-native were used for developing this app. 

**USER STORIES:**

As a user I want to search the place and get the live weather conditions of that place.

**ACCEPTANCE CRITERIA:**
* Test if the user can be able to search a place and find it's weather condition.
* Test if app alerts users if they try to search an empty place.
* Test the place entered by user is invalid and alert if it's an invalid place.
* Test if app alerts the user if the place name entered is not valid anymore or if it's changed as invalid place.
* Test if there is some animation or loading message if there is delay in fetching the weather.
* Test if the button is disabled after searching and until the weather is fetched or error is alerted.