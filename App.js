import React, {useState} from 'react';
import { StyleSheet, Text, View , TextInput , Button , ActivityIndicator , Modal ,SafeAreaView} from 'react-native';

const getCelsiusFromKelvin = (tempInKelvin) => Math.floor(tempInKelvin - 273.15,2);

const WeatherWidget = (props) => {
  const { weatherData } = props;

  return (
    <View style={styles.weatherContainer }>
      <Text>{weatherData.weather[0].main} : {weatherData.weather[0].description} </Text>
      <Text>Temperature : {getCelsiusFromKelvin(weatherData.main.temp)} °C</Text>
      <Text>Temperature feels like : {getCelsiusFromKelvin(weatherData.main.feels_like)} °C</Text>
      <Text>Temperature maximum : {getCelsiusFromKelvin(weatherData.main.temp_max)} °C</Text>
      <Text>Temperature minimum : {getCelsiusFromKelvin(weatherData.main.temp_min)} °C</Text>
      <Text>Pressure : {weatherData.main.pressure} hPa</Text>
      <Text>Humidity : {weatherData.main.humidity} %</Text>
      <Text>Wind Speed : {weatherData.wind.speed} m/s</Text>
      <Text>Latitude : {weatherData.coord.lat} </Text>
      <Text>Longitude : {weatherData.coord.lon} </Text>
    </View>
  );
}

 function App() {
  const [currentCity, setCity] = useState('');
  const [currentWeather, setCurrentWeather] = useState(null);
  const [ fetching , setFetching ] = useState(false);
  const [ modal , setModal ] = useState(false);
  
  const clickHandler = () => {
    if(currentCity === '') {
      alert('Please enter the City');
      return;
    }

    setFetching(true);
    const api = `http://api.openweathermap.org/data/2.5/weather?q=${currentCity}&appid=edd5e08580587410a8555ca6279ab520`
    
    fetch(api)
    .then(response => response.json())
    .then(json => {
      setFetching(false);

      if (json.message === 'city not found') {
        alert('Invalid city');  
      } else {
        setCurrentWeather(json);
      }
    }, err => {
      setFetching(false);
      alert('Something went wrong');
    });
  }

  const cityInputHandler = (city) => {
    setCity(city);
  }

  return (
    <View style = {styles.screen}>

      <Modal visible={modal} animationType="slide">

        <SafeAreaView>

          <View style={{justifyContent:"center" , alignItems:"center"}}>

            <View style={styles.headingContainer}>
              <Text style={{alignItems:"center" ,fontSize : 25,fontWeight : "bold"}}>
                Weather App
              </Text>
            </View>

            <Button title="Back" onPress={() => setModal(false)} />

            <View style={{margin : 20}}>
              <Text> This is Weather App which gives the weather conditions of the place you enter. This was developed as a learning part by Bharath.</Text>
            </View>

          </View>

        </SafeAreaView>

      </Modal>

      <View style={styles.headingContainer}>
        <Text style={{alignItems:"center" ,fontSize : 25,fontWeight : "bold"}}>
          Weather App
        </Text>
      </View>

      <Button title="i" onPress={() => setModal(true)} />

      <View style={styles.inputContainer}>
        <TextInput placeholder="Enter city..." style={{ width:'80%',borderColor: 'black',
         borderWidth : .5, padding : 8 }} onChangeText={cityInputHandler} value ={currentCity}/>
        {
          !fetching && <Button title="Search" style={{borderRadius:5}} onPress={clickHandler}/>
        }
      </View>

      <View>
        {
          fetching && <ActivityIndicator size="large" color="black" animating={fetching} /> 
        }
        
        {
          currentWeather && <WeatherWidget weatherData={currentWeather} />
        }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen : {margin : 50},
  headingContainer : {alignItems:"center", borderColor : "black" ,borderWidth : 1, padding : 10, 
                      width : "100%", backgroundColor : "lightgrey"},
  weatherContainer : { margin : "auto" ,borderColor : "black" , padding : 30, 
                      width : "100%" , borderWidth : 1, backgroundColor : "#d4d6ca", alignItems:"center"},
  inputContainer: {flexDirection : 'row' , justifyContent : 'space-between', alignItems : 'center', padding: 20},
  listItem : {padding :10, margin : 3 , backgroundColor : '#ccc',
  borderColor : 'black',
  borderWidth : 1}
});

export default App;